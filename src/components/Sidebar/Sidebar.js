import React from "react";
import "./Sidebar.css";
import ChatIcon from "@material-ui/icons/Chat";
import DonutLargeIcon from "@material-ui/icons/DonutLarge";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import { IconButton, Avatar } from "@material-ui/core";
import SidebarChat from "../SidebarChat/SidebarChat";
import { useState, useEffect } from "react";
import { useAuth } from "../../contexts/AuthContext";
import apiChat from "../../services/apiChat";

const Sidebar = ({ conversations }) => {
  const { idUser, saveConversations,saveMessages,saveLiveConversation } = useAuth();
  const [newUsersConversations, setNewUsersConversations] = useState([]);
  const [conversationView, setConversationView] = useState(null);
  const [newConversationsView, setNewConversationsView] = useState(null);


  const conversationHandler = (event) => {
    apiChat
      .createConversation(idUser, event.target.value)
      .then((response) => {
        if (response.status === 400 || response.status === 500) {
          throw new Error(response.text());
        } else {
          apiChat.getConversationsByUserId(idUser).then((conversations) => {
            saveConversations(conversations);
          });
        }
      })
      .catch((error) => console.error(error));
  };

  const conversationClickHandler = (event) => {
 
    apiChat.getMessages(event.target.closest(".select-conversation").getAttribute("value")).then((response) => response.json()).then((conversation) => {
      saveLiveConversation(conversation);
      saveMessages (conversation[0].messages); 

   
    }).catch((error) => console.error(error));

  };

  useEffect(() => {
    if (conversations) {
      apiChat.getNewUsersConversations(idUser).then((users) => {
;
 
        setNewUsersConversations(users);
      });

      let names = conversations.map((conversation) => {
        return (
          <div key={conversation._id} value={conversation._id} onClick={conversationClickHandler} className="select-conversation">
            <SidebarChat
              name={
                conversation.membersObj.filter(
                  (member) => member._id !== idUser
                )[0].username
              }
            ></SidebarChat>
          </div>
        );
      });
      setConversationView(names);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [conversations, idUser]);

  useEffect(() => {
    if (newUsersConversations) {
      let options = newUsersConversations.map((user) => {
        return (
          <option key={user._id} value={user._id}>
            {user.username}
          </option>
        );
      });
      setNewConversationsView(options);
    }
  }, [newUsersConversations]);

  return (
    <div className="sidebar">
      <div className="sidebar__header">
        <Avatar src="" />
        <div className="sidebar__headerRight">
          <IconButton>
            <DonutLargeIcon />
          </IconButton>
          <IconButton>
            <ChatIcon />
          </IconButton>
          <IconButton>
            <MoreVertIcon />
          </IconButton>
        </div>
      </div>
      <div className="sidebar__search">
        <div className="sidebar__searchContainer">
          <div id="select-conversation">
            <select
              onChange={conversationHandler}
              name="conversations"
              id="conversations"
            >
              <option value="">Establezca una nueva conversación: </option>
              {newConversationsView}
            </select>
          </div>
        </div>
      </div>
      <div className="sidebar__chats">{conversationView}</div>
    </div>
  );
};

export default Sidebar;
