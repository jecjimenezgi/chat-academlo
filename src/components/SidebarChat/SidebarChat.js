import { Avatar } from "@material-ui/core";
import React from "react";
import "./SidebarChat.css";

const SidebarChat = ({name}) => {
  return (
    <>
    <div className="sidebarChat">
      <Avatar />
      <div className="sidebarChat__info">
        <h2>{name}</h2>
      </div>
    </div>
    </>
  );
};
export default SidebarChat;
