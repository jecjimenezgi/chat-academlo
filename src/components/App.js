import "./App.css"
import "./Grid.css"
import React from "react"
import Signup from "./Signup"
import { AuthProvider } from "../contexts/AuthContext"
import { BrowserRouter as Router, Switch, Route,Redirect} from "react-router-dom"
import Login from "./Login"
import PrivateRoute from "./PrivateRoute"
import ForgotPassword from "./ForgotPassword"
import UpdateProfile from "./UpdateProfile"
import Dashboard from "./Dashboard"

function App() {
  return ( 
      <div className="App">
        <Router>
          <AuthProvider>
            <Switch>
              <PrivateRoute path="/chat" component={Dashboard} />
              <PrivateRoute path="/update-profile" component={UpdateProfile} />
              <div className="intro">
              <Route path="/signup" component={Signup} />
              <Route path="/login" component={Login} />
              <Route path="/forgot-password" component={ForgotPassword} />
              <Route path="/">
                <Redirect to="/login"></Redirect>
              </Route>
              </div>
            </Switch>
          </AuthProvider>
        </Router>
      </div>
  )
}

export default App
