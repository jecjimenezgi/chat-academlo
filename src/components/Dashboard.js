import "./Dashboard.css";
import React, { useEffect } from "react";
import Profile from "./Profile";
import Chat from "./Chat/Chat";
import Sidebar from "./Sidebar/Sidebar";

import { useAuth } from "../contexts/AuthContext";
import apiChat from "../services/apiChat"



const Dashboard = () => {

  const { idUser,conversations, saveConversations, messages } = useAuth()




  useEffect( () => {
    if (idUser ) {
      apiChat.getConversationsByUserId(idUser).then(conversations => {
        
        saveConversations(conversations)
      })
    }
      // eslint-disable-next-line react-hooks/exhaustive-deps
  } , [idUser])



  

  return (
 <>
    <div className="Dashboard">
      <div className="row">
      <div className="col-12">
           <Profile></Profile> 
        </div>
        <div className="col-12">
          <div className="dash">
            <div className="dash__body">
              <Sidebar conversations={conversations}/>
              <Chat messages={messages} />
            </div>
          </div>
        </div>
      </div>
    </div>
    </>
  );
};

export default Dashboard;
