import React, { useState, useEffect } from "react";
import { Avatar, IconButton } from "@material-ui/core";
import {
  AttachFile,
  InsertEmoticon,
  MoreVert,
  SearchOutlined,
} from "@material-ui/icons";
import "./Chat.css";
import MicIcon from "@material-ui/icons/Mic";
import { useAuth } from "../../contexts/AuthContext";
import apiChat from "../../services/apiChat";


const Chat = ({ messages }) => {

  const {idUser, liveConversation, saveMessages} = useAuth();

  const [input, setInput] = useState("");

  const [chatName, setChatName] = useState("");

  

  useEffect(() => {

    if (liveConversation) {
      let user = liveConversation[0].members.filter( (member) => idUser !== member )[0]
      apiChat.getUserById(user).then( (response) => response.json() ).then( (user) => {
        setChatName(user.firstName + " " + user.lastName)
      })

    }
    
  } , [liveConversation, idUser])

  const sendMessage = async (e) => {
    e.preventDefault();
    apiChat.postMessage(idUser, liveConversation[0]._id, input).then( response => {
      if (response.status === 200) {
        console.log("mensagem enviada");
        apiChat.getMessages(liveConversation[0]._id).then((response) => response.json()).then((conversation) => {

  
        saveMessages (conversation[0].messages); 
    
       
        }).catch((error) => console.error(error));
    
        setInput("");
      }else {
        console.log(response)
        console.log("mensagem não enviada");
      }
    }).catch( (error) => console.error(error) );
  };
  
  return (
    <>
    <div className="chat">
      <div className="chat__header">
        <Avatar />
        <div className="chat__headerInfo">
          <h3>{chatName}</h3>  
        </div>
        <div className="chat__headerRight">
          <IconButton>
            <SearchOutlined />
          </IconButton>
          <IconButton>
            <AttachFile />
          </IconButton>
          <IconButton>
            <MoreVert />
          </IconButton>
        </div>
      </div>

      <div className="chat__body">
        {messages.map((message, i) => {
          if (message.userId === idUser){
            return (
              <div key={i} className="me">
              <p  className="chat__message">
                <span className="chat__name">{message.userName}</span>
                {message.message}
                <span className="chat__timestamp">{message.timestamp}</span>
                <span className="chat__timestamp">(Yo)</span>
              </p>
              </div>
            );
          }
          return (
            <p
              key={i}
              className={`chat__message ${
                message.received && "chat__reciever"
              }`}
            >
              <span className="chat__name">{message.name}</span>
              {message.message}
              <span className="chat__timestamp">{message.timestamp}</span>
            </p>
          );
        })}
      </div>

      <div className="chat__footer">
        <InsertEmoticon />
        <form>
          <input
            value={input}
            onChange={(e) => {
              setInput(e.target.value);
            }}
            type="text"
            placeholder="Escribe un mensaje"
          />
          <button onClick={sendMessage} type="submit">
            Enviar
          </button>
        </form>
        <MicIcon />
      </div>
    </div>
    </>
  );
};

export default Chat;
