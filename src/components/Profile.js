import "./Profile.css"
import React, { useState } from "react"
import { Card, Button, Alert } from "react-bootstrap"
import { useAuth } from "../contexts/AuthContext"
import { Link, useHistory } from "react-router-dom"
import apiChat from "../services/apiChat"

export default function Profile() {
  const [error, setError] = useState("")
  const { currentUser, logout,idUser,saveIdUser } = useAuth()
  const history = useHistory()


    if (currentUser) {
      apiChat.getUserByEmail(currentUser.email).then(user => {
        saveIdUser(user._id)
      })
    }


  async function handleLogout() {
    setError("")

    try {
      await logout()
      history.push("/login")
    } catch {
      setError("Failed to log out")
    }
  }

  return (
    <div className="Profile">
    <>
      <Card>
        <Card.Body>
          <h2 className="text-center mb-4">Perfil</h2>
          {error && <Alert variant="danger">{error}</Alert>}
          <strong>Email:</strong> {currentUser.email} - {idUser}
          <Link to="/update-profile" className="btn btn-primary w-100 mt-3">
            Actualizar contraseña
          </Link>
        </Card.Body>
      </Card>
      <div className="w-100 text-center mt-2">
        <Button variant="link" onClick={handleLogout}>
          Cerrar sesión
        </Button>
      </div>
    </>
    </div>
  )
}
