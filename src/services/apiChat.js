const postUser = (firstName, lastName, email, username) => {
  var myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/json");

  var raw = JSON.stringify({
    firstName: firstName,
    lastName: lastName,
    email: email,
    username: username,
  });

  var requestOptions = {
    method: "POST",
    headers: myHeaders,
    body: raw,
    redirect: "follow",
  };

  return fetch(
    "https://academlo-whats.herokuapp.com/api/v1/users",
    requestOptions
  );
};

const getUsers = () => {
  var requestOptions = {
    method: "GET",
    redirect: "follow",
  };

  return fetch(
    "https://academlo-whats.herokuapp.com/api/v1/users",
    requestOptions
  );
};

const getConversations = (id) => {
  var requestOptions = {
    method: "GET",
    redirect: "follow",
  };

  return fetch(
    `https://academlo-whats.herokuapp.com/api/v1/users/${id}/conversations`,
    requestOptions
  );
};

const getUserByEmail = (email) => {
  return getUsers()
    .then((response) => response.json())
    .then((users) => {
      return users.filter((user) => user.email === email)[0];
    })
    .catch((error) => {
      console.error(error);
    });
};

const getConversationsByUserId = (id) => {
  return getConversations(id)
    .then((response) => response.json())
    .then((conversations) => {
      return conversations.filter((conversation) =>
        conversation.members.includes(id)
      );
    })
    .catch((error) => {
      console.error(error);
    });
};

const createConversation = (id, otherId) => {
  var myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/json");

  var raw = JSON.stringify({
    members: [id, otherId],
  });

  var requestOptions = {
    method: "POST",
    headers: myHeaders,
    body: raw,
    redirect: "follow",
  };

  return fetch(
    "https://academlo-whats.herokuapp.com/api/v1/conversations",
    requestOptions
  );
};

async function getNewUsersConversations(id) {


  let users = await getUsers().then((response) => response.json());


  let members = await getConversationsByUserId(id).then((conversations) =>
    conversations.map((conversation) => conversation.members)
  );

  if (members.length === 0) {
    return users;
  }
  let new_users = users.filter((user) => {
    if (members.flat().length !== 0) {
      return !members.flat().includes(user._id);
    } else {
      return false;
    }
  });

  return new_users;
}

const getMessages = (conversationId) => {
  var requestOptions = {
    method: "GET",
    redirect: "follow",
  };

  return fetch(
    `https://academlo-whats.herokuapp.com/api/v1/conversations/${conversationId}/messages`,
    requestOptions
  );
};

const postMessage = (userId, conversationId, message) => {
  var myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/json");

  var raw = JSON.stringify({
    userId: userId,
    conversationId: conversationId,
    message: message,
    timestamp: new Date().getTime(),
    received: false,
  });

  var requestOptions = {
    method: "POST",
    headers: myHeaders,
    body: raw,
    redirect: "follow",
  };

  return fetch(
    "https://academlo-whats.herokuapp.com/api/v1/messages/",
    requestOptions
  );
};


const getUserById = (id) => {
  var requestOptions = {
    method: 'GET',
    redirect: 'follow'
  };
  
  return fetch(`https://academlo-whats.herokuapp.com/api/v1/users/${id}`, requestOptions)
}

const apiChat = {
  getUserById: getUserById,
  getMessages: getMessages,
  postMessage: postMessage,
  createConversation: createConversation,
  getConversations: getConversations,
  getNewUsersConversations: getNewUsersConversations,
  getConversationsByUserId: getConversationsByUserId,
  postUser: postUser,
  getUsers: getUsers,
  getUserByEmail: getUserByEmail,
};

export default apiChat;
